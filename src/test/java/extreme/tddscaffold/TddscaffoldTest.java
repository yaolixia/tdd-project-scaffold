package extreme.tddscaffold;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;

public class TddscaffoldTest {
    @Test
    public void should_return_9_19_when_commodity_is_general_merchandise_and_not_overdue(){
        Tddscaffold tddscaffold = new Tddscaffold();
        String result = tddscaffold.say("General_Merchandise",10,20);
        Assertions.assertEquals("9,19",result);
    }

    @Test
    public void should_return_1_0_when_commodity_is_general_merchandise_and_not_overdue_and_quality_is_0(){
        Tddscaffold tddscaffold = new Tddscaffold();
        String result = tddscaffold.say("General_Merchandise",2,0);
        Assertions.assertEquals("1,0",result);
    }

    @Test
    public void should_return_2_5_when_commodity_is_general_merchandise_and_not_overdue(){
        Tddscaffold tddscaffold = new Tddscaffold();
        String result = tddscaffold.say("General_Merchandise",3,6);
        Assertions.assertEquals("2,5",result);
    }

    @Test
    public void should_return_negative1_4_when_commodity_is_general_merchandise_and_overdue(){
        Tddscaffold tddscaffold = new Tddscaffold();
        String result = tddscaffold.say("General_Merchandise",0,6);
        Assertions.assertEquals("-1,4",result);
    }

    @Test
    public void should_return_negative2_4_when_commodity_is_general_merchandise_and_overdue(){
        Tddscaffold tddscaffold = new Tddscaffold();
        String result = tddscaffold.say("General_Merchandise",-1,6);
        Assertions.assertEquals("-2,4",result);
    }

    @Test
    public void should_return_14_21_when_commodity_is_Backstage_pass_and_Shelf_life_greater_than_10(){
        Tddscaffold tddscaffold = new Tddscaffold();
        String result = tddscaffold.say("Backstage_Pass",15,20);
        Assertions.assertEquals("14,21",result);
    }

    @Test
    public void should_return_9_47_when_commodity_is_Backstage_pass_and_Shelf_life_less_than_10(){
        Tddscaffold tddscaffold = new Tddscaffold();
        String result = tddscaffold.say("Backstage_Pass",10,45);
        Assertions.assertEquals("9,47",result);
    }

    @Test
    public void should_return_8_47_when_commodity_is_Backstage_pass_and_Shelf_life_less_than_10(){
        Tddscaffold tddscaffold = new Tddscaffold();
        String result = tddscaffold.say("Backstage_Pass",9,45);
        Assertions.assertEquals("8,47",result);
    }

    @Test
    public void should_return_9_50_when_commodity_is_Backstage_pass_and_Shelf_life_less_than_10(){
        Tddscaffold tddscaffold = new Tddscaffold();
        String result = tddscaffold.say("Backstage_Pass",10,49);
        Assertions.assertEquals("9,50",result);
    }

    @Test
    public void should_return_9_50_when_commodity_is_Backstage_pass_and_Shelf_life_less_than_10$2(){
        Tddscaffold tddscaffold = new Tddscaffold();
        String result = tddscaffold.say("Backstage_Pass",10,50);
        Assertions.assertEquals("9,50",result);
    }

    @Test
    public void should_return_4_50_when_commodity_is_Backstage_pass_and_Shelf_life_less_than_5$2(){
        Tddscaffold tddscaffold = new Tddscaffold();
        String result = tddscaffold.say("Backstage_Pass",5,49);
        Assertions.assertEquals("4,50",result);
    }

    @Test
    public void should_return_4_48_when_commodity_is_Backstage_pass_and_Shelf_life_less_than_5(){
        Tddscaffold tddscaffold = new Tddscaffold();
        String result = tddscaffold.say("Backstage_Pass",5,45);
        Assertions.assertEquals("4,48",result);
    }

    @Test
    public void should_return_0_23_when_commodity_is_Backstage_pass_and_Shelf_life_less_than_5(){
        Tddscaffold tddscaffold = new Tddscaffold();
        String result = tddscaffold.say("Backstage_Pass",1,20);
        Assertions.assertEquals("0,23",result);
    }

    @Test
    public void should_return_0_23_when_commodity_is_Backstage_pass_and_overdue(){
        Tddscaffold tddscaffold = new Tddscaffold();
        String result = tddscaffold.say("Backstage_Pass",0,20);
        Assertions.assertEquals("-1,0",result);
    }

    @Test
    public void should_return_0_45_when_commodity_is_Sulfura_and_not_overdue(){
        Tddscaffold tddscaffold = new Tddscaffold();
        String result = tddscaffold.say("Sulfura",0,45);
        Assertions.assertEquals("0,45",result);
    }

    @Test
    public void should_return_negative1_45_when_commodity_is_Sulfura_and_overdue(){
        Tddscaffold tddscaffold = new Tddscaffold();
        String result = tddscaffold.say("Sulfura",-1,45);
        Assertions.assertEquals("-1,45",result);
    }

    @Test
    public void should_return_negative1_50_when_commodity_is_Sulfura_and_overdue(){
        Tddscaffold tddscaffold = new Tddscaffold();
        String result = tddscaffold.say("Sulfura",-1,50);
        Assertions.assertEquals("-1,50",result);
    }

    @Test
    public void should_return_negative1_1_when_commodity_is_Sulfura_and_overdue(){
        Tddscaffold tddscaffold = new Tddscaffold();
        String result = tddscaffold.say("Sulfura",-1,1);
        Assertions.assertEquals("-1,1",result);
    }

    @Test
    public void should_return_1_1_when_commodity_is_Aged_Bri_and_not_overdue(){
        Tddscaffold tddscaffold = new Tddscaffold();
        String result = tddscaffold.say("Aged_Bri",2,0);
        Assertions.assertEquals("1,1",result);
    }

    @Test
    public void should_return_1_50_when_commodity_is_Aged_Bri_and_not_overdue(){
        Tddscaffold tddscaffold = new Tddscaffold();
        String result = tddscaffold.say("Aged_Bri",2,49);
        Assertions.assertEquals("1,50",result);
    }

    @Test
    public void should_return_2_50_when_commodity_is_Aged_Bri_and_not_overdue(){
        Tddscaffold tddscaffold = new Tddscaffold();
        String result = tddscaffold.say("Aged_Bri",1,50);
        Assertions.assertEquals("0,50",result);
    }

    @Test
    public void should_return_negative1_22_when_commodity_is_Aged_Bri_and_overdue(){
        Tddscaffold tddscaffold = new Tddscaffold();
        String result = tddscaffold.say("Aged_Bri",0,20);
        Assertions.assertEquals("-1,22",result);
    }

    @Test
    public void should_return_negative2_22_when_commodity_is_Aged_Bri_and_overdue(){
        Tddscaffold tddscaffold = new Tddscaffold();
        String result = tddscaffold.say("Aged_Bri",-1,20);
        Assertions.assertEquals("-2,22",result);
    }
}
