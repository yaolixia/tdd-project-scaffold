# TDD

## 开发环境
 - JDK11+
 
## 业务目标

## Tasking

## 编码路线


## 参考资料
- [JUnit 5用户指南](https://gitee.com/liushide/junit5_cn_doc/blob/master/junit5UserGuide_zh_cn.md#https://gitee.com/link?target=https%3A%2F%2Fgithub.com%2Fjunit-team%2Fjunit5-samples%2Ftree%2Fr5.0.2%2Fjunit5-gradle-consumer)
- [Gradle 用户指南](https://docs.gradle.org/current/userguide/userguide.html)

/*普通商品*/
Given sellIn为10，quality为20 商品为普通商品且未过期 When 出售 Then 报 updatedSellIn为9，updatedQuality为19
Given sellIn为2，quality为0 商品为普通商品且未过期，商品价值为零 When 出售 Then 报 updatedSellIn为1，updatedQuality为0
Given sellIn为3，quality为6 商品为普通商品且未过期 When 出售 Then 报 updatedSellIn为2，updatedQuality为5
Given sellIn为0，quality为6 商品为普通商品且过期 When 出售 Then 报 updatedSellIn为-1，updatedQuality为4
Given sellIn为-1，quality为6 商品为普通商品且过期 When 出售 Then 报 updatedSellIn为-2，updatedQuality为4

/*后台门票*/
Given sellIn为15，quality为20 商品为后台门票且保质期大于10天 When 出售 Then 报 updatedSellIn为14，updatedQuality为21
Given sellIn为10，quality为45 商品为后台门票且保质期等于10天，商品价值为零 When 出售 Then 报 updatedSellIn为9，updatedQuality为47
Given sellIn为9，quality为45 商品为后台门票且保质期小于10天 When 出售 Then 报 updatedSellIn为8，updatedQuality为47
Given sellIn为10，quality为49 商品为后台门票且保质期小于10天 When 出售 Then 报 updatedSellIn为9，updatedQuality为50
Given sellIn为10，quality为50 商品为后台门票且保质期小于10天，商品价值为50 When 出售 Then 报 updatedSellIn为9，updatedQuality为50
Given When sellIn为5，quality为49 商品为后台门票且保质期小于5天 出售 Then 报 updatedSellIn为4，updatedQuality为50
Given sellIn为5，quality为45 商品为后台门票且保质期小于5天 When 出售 Then 报 updatedSellIn为4，updatedQuality为48
Given sellIn为1，quality为20 商品为后台门票且保质期小于5天 When 出售 Then 报 updatedSellIn为0，updatedQuality为23
Given sellIn为0，quality为20 商品为后台门票且过期 When 出售 Then 报 updatedSellIn为-1，updatedQuality为0

/*萨弗拉斯*/
Given sellIn为0，quality为45 商品为萨弗拉斯且未过期 When 出售 Then 报 updatedSellIn为0，updatedQuality为45
Given sellIn为-1，quality为45 商品为萨弗拉斯且过期 When 出售 Then 报 updatedSellIn为-1，updatedQuality为45
Given sellIn为-1，quality为50 商品为萨弗拉斯且过期，商品价值为50 When 出售 Then 报 updatedSellIn为-1，updatedQuality为50
Given sellIn为-1，quality为1 商品为萨弗拉斯且过期 When 出售 Then 报 updatedSellIn为-1，updatedQuality为1
Given sellIn为-2，quality为1 商品为萨弗拉斯且过期 When 出售 Then 报 updatedSellIn为-2，updatedQuality为1

/*陈年干酪*/
Given sellIn为2，quality为0 商品为陈年干酪且未过期 When 出售 Then 报 updatedSellIn为1，updatedQuality为1
Given sellIn为2，quality为49 商品为陈年干酪且未过期 When 出售 Then 报 updatedSellIn为1，updatedQuality为50
Given sellIn为2，quality为50 商品为陈年干酪且未过期且商品价值为50 When 出售 Then 报 updatedSellIn为1，updatedQuality为50
Given sellIn为0，quality为20 商品为陈年干酪且过期 When 出售 Then 报 updatedSellIn为-1，updatedQuality为22
Given sellIn为-1，quality为20 商品为陈年干酪且过期 When 出售 Then 报 updatedSellIn为-2，updatedQuality为22
